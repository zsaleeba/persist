#ifndef RUN_H
#define RUN_H

#include <vector>
#include <memory>


// Forward declarations.
class Bin;
class Page;


//
// Runs are sets of one or more contiguous pages.
// They're aligned to the page size.
//

namespace persist
{

class Run
{
private:
    std::unique_ptr<Bin> bin; // bins are further down in our discussion.
    size_t regionSize; // the region size for this run.
    std::vector<Page> pages; // pages that are a part of this run. 
    int numFreeRegions;
    int totalNumRegions;
    std::vector<bool> freeRegions; // initially all set to true.
    
public:
    Run();
};

} // namespace persist

#endif // RUN_H
