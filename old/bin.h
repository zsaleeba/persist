#ifndef BIN_H
#define BIN_H

#include <vector>
#include <memory>


// Forward declarations.
class Run;


//
// Chunks are sections of memory we can allocate into.
//


namespace persist
{

class Bin
{
private:
    size_t binRegionSize_;
    std::vector<Run> runs_;
    std::weak_ptr<Run> currentRun_; // Current run of this bin.
    
public:
    Bin();
};

} // namespace persist

#endif // BIN_H
