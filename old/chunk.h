#ifndef CHUNK_H
#define CHUNK_H

#include <vector>
#include <map>
#include <memory>


namespace persist
{

// Forward declarations.
class Arena;
class Run;
class Page;


//
// Chunks are sections of memory we can allocate into.
//

class Chunk
{
private:
    std::weak_ptr<Arena> arena_; // Arena that this chunk is associated with.
    bool dirtied_;               // This is set if the chunk has ever been dirtied.
    std::vector<Run> runs_;      
    std::multimap<size_t, Page> freePages_; // Metadata about free pages for a region size.
    
public:
    Chunk();
};

} // namespace persist

#endif // CHUNK_H
