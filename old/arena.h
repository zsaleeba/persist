#ifndef ARENA_H
#define ARENA_H

#include <vector>
#include <map>
#include <thread>
#include <mutex>

namespace persist
{

// Forward declarations.
class Chunk;
class Run;
class Bin;

//
// Arenas map threads to sections of memory.
//

class Arena
{
private:
    std::vector<std::thread::id> threads_;
    std::vector<Chunk> chunks_;
    std::mutex lock_;
    std::map<size_t, Run> cleanRuns_;
    std::map<size_t, Run> dirtyRuns_;
    std::vector<Bin> bins_;
    
public:
    Arena();
};

} // namespace persist

#endif // ARENA_H
