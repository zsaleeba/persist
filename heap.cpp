#include <thread>
#include <experimental/filesystem>
#include <algorithm>
#include <memory>

#include "heap.h"
#include "segment.h"
#include "runtimeexception.h"


namespace deltacode {


//
// Constructor.
//

Heap::Heap(const std::string &path, const std::string &prefix, const std::string &extension, size_t indexBlockSize, bool clearStore, size_t initialFileSize) :
    path_(path), prefix_(prefix), extension_(extension), nextSegmentLevel_(DEFAULT_ALLOC_LEVEL), nextSegmentId_(0)
{
    // Do we need to clear any existing data?
    if (clearStore)
    {
        deleteDataStore();
    }

    // Is there an existing data store to open?
    Segment firstSegment(path, prefix, extension, 0);
    if (!firstSegment.exists())
    {
        // Create the new data store.
        createDataStore(initialFileSize);
    }

    // Load it.
    loadDataStore();

    // Get the index block.
    getIndexBlockFirstTime(indexBlockSize);
}


//
// Delete the entire old data store.
//

void Heap::deleteDataStore()
{
    // Try to get information about an existing persistent data store.
    int numSegmentFiles = -1;

    try {
        Segment oldSeg(path_, prefix_, extension_, 0);

        oldSeg.load();
        numSegmentFiles = oldSeg.getStoreMaxLevel() - oldSeg.getMinLevel() + 1;
    }
    catch (const RuntimeException &e) {
        // Couldn't read an existing data store, there's probably nothing there.
        return;
    }

    // Delete all the old files.
    for (int i = 0; i < numSegmentFiles; i++)
    {
        Segment seg(path_, prefix_, extension_, i);
        seg.deleteFile();
    }
}


//
// Load the entire old data store.
//

void Heap::loadDataStore()
{
    // Try to get information about an existing persistent data store.
    int numSegmentFiles = -1;

    segments_.clear();
    lastAllocatedBucket_.clear();
    
    try {
        std::unique_ptr<Segment> firstSegment = std::make_unique<Segment>(path_, prefix_, extension_, 0);
        firstSegment->load();
        numSegmentFiles = firstSegment->getNumSegments();
        nextSegmentLevel_ = firstSegment->getSegmentMaxLevel() + 1;
        nextSegmentId_ = numSegmentFiles;
        segments_.push_back(std::move(firstSegment));
    }
    catch (const RuntimeException &e) {
        // Couldn't read an existing data store, there's probably nothing there.
        return;
    }

    // Map in all the remaining segment files.
    for (int i = 1; i < numSegmentFiles; i++)
    {
        std::unique_ptr<Segment> segment = std::make_unique<Segment>(path_, prefix_, extension_, i);
        segment->load();
        segments_.push_back(std::move(segment));
    }
}


//
// Create an initial data store.
//

void Heap::createDataStore(size_t initialFileSize)
{
    // We only make files in power of two sizes.
    size_t realFileSize = roundUpToPowerOfTwo(initialFileSize);
    int    realLevel    = log2PowerOfTwo(realFileSize);

    if (realLevel < MIN_ALLOC_LEVEL)
    {
        realLevel = MIN_ALLOC_LEVEL;
    }

    // Create the first segment.
    std::unique_ptr<Segment> firstSegment = std::make_unique<Segment>(path_, prefix_, extension_, 0);
    firstSegment->create(MIN_ALLOC_LEVEL, realLevel, realLevel);
    segments_.push_back(std::move(firstSegment));

    nextSegmentId_ = 1;
    nextSegmentLevel_ = realLevel + 1;
}


//
// Allocate some memory.
//

void *Heap::allocate(size_t len)
{
    // Work out what level we're going to allocate at.
    size_t allocSize = roundUpToPowerOfTwo(len);
    int level = log2PowerOfTwo(allocSize);
    if (level < MIN_ALLOC_LEVEL)
    {
        level = MIN_ALLOC_LEVEL;
    }

    // Wait for exclusive access.
    std::lock_guard<std::mutex> lock(mutex_);

    // Have we allocated anything of this size before?
    if (static_cast<int>(lastAllocatedBucket_.size()) < level + 1)
    {
        // Extend the vector and fill with -1.
        size_t oldVecSize = lastAllocatedBucket_.size();
        lastAllocatedBucket_.resize(level + 1);
        std::fill(&lastAllocatedBucket_[oldVecSize], &lastAllocatedBucket_[level + 1], -1);
    }

    int startAt = lastAllocatedBucket_[level];

    // Try allocating in the same segment again.
    if (startAt >= 0)
    {
        // We have a hint of what size to start at.
        void *result = segments_.at(startAt)->allocate(level);
        if (result)
        {
            // It allocated ok in the same segment as last time.
            return result;
        }
    }

    // Either we had no previous segment for this size or it's filled up. Try all the segments.
    for (int segNo = 0; segNo < static_cast<int>(segments_.size()); segNo++)
    {
        if (segNo != startAt)
        {
            void *result = segments_.at(segNo)->allocate(level);
            if (result)
            {
                // It allocated ok. Note down the segment for later.
                lastAllocatedBucket_[level] = segNo;

                return result;
            }
        }
    }

    // Couldn't allocate it in any of the existing segments. Make a larger one which it'll fit in.
    int newSegmentLevel = std::max(level + LARGE_ALLOC_MULTIPLE, nextSegmentLevel_);
    std::unique_ptr<Segment> newSegment = std::make_unique<Segment>(path_, prefix_, extension_, nextSegmentId_);
    newSegment->create(MIN_ALLOC_LEVEL, newSegmentLevel, newSegmentLevel);
    newSegment->load();
    segments_.push_back(std::move(newSegment));

    nextSegmentLevel_ = newSegmentLevel + 1;
    nextSegmentId_++;

    // Update the index.
    std::unique_ptr<Segment> &fs = segments_.front();
    fs->setStoreMaxLevel(newSegmentLevel);
    fs->setNumSegments(nextSegmentId_);

    // Now allocate it in the new segment.
    return segments_.back()->allocate(level);
}


//
// Free some previously allocated memory.
//

void Heap::free(void *mem)
{
    // Wait for exclusive access.
    std::lock_guard<std::mutex> lock(mutex_);

    // Find the segment with this item.
    for (auto &s : segments_)
    {
        if (s->isInMemoryRange(mem))
        {
            s->free(mem);
            return;
        }
    }

    throw RuntimeException("freeing non-persistent memory range");
}


//
// Get the index block of memory.
//

void Heap::getIndexBlockFirstTime(size_t len)
{
    size_t allocSize = roundUpToPowerOfTwo(len);
    int level = log2PowerOfTwo(allocSize);
    indexBlock_ = segments_.front()->getIndexBlock(level);
}


//
// Return the next power two which is greater than or equal to the given number.
//

uint64_t Heap::roundUpToPowerOfTwo(uint64_t v) const
{
    v--;

    for (int i = 0; i < 6; i++)
    {
        v |= v >> (1 << i);
    }

    return v + 1;
}


//
// Find the bit position of a power of two value.
//

int Heap::log2PowerOfTwo(uint64_t v) const
{
    static const size_t b[] = { 0xAAAAAAAAAAAAAAAA, 0xCCCCCCCCCCCCCCCC, 0xF0F0F0F0F0F0F0F0, 
                                0xFF00FF00FF00FF00, 0xFFFF0000FFFF0000, 0xFFFFFFFF00000000 };
    int r = (v & b[0]) != 0;
    for (int i = 5; i > 0; i--)
    {
        r |= ((v & b[i]) != 0) << i;
    }

    return r;
}

} // namespace deltacode
