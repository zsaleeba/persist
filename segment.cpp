//
// A heap is made of a number of segments. Segments are areas of memory
// which are used for allocation. A heap usually contains multiple
// segments of different sizes. 
//
// Segments correspond to files on the disk used as backing store.
//
// Each time the heap runs out of space it allocates a new segment of a
// larger size than all the previous segments.
//

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <cstdint>
#include <experimental/filesystem>
 
#include "persist.h"
#include "segment.h"
#include "runtimeexception.h"

namespace fs = std::experimental::filesystem;


// File version numbers for this version of the PDB file.
#define DELTACODE_PDB_MAGIC         "DeltaCode"
#define DELTACODE_PDB_VERSION_MINOR 0
#define DELTACODE_PDB_VERSION_MAJOR 1


namespace deltacode {


//
// Open an existing segment.
//

Segment::Segment(const std::string &path, const std::string &prefix, const std::string &extension, int segmentId) :
    minLevel_(0), segmentMaxLevel_(0), storeMaxLevel_(0), fd_(-1), mappedMem_(nullptr), mappedSize_(0), spaceUsed_(0), blockStatus_(nullptr), blockMem_(nullptr)
{
    // Construct the full file path.
    std::stringstream fileNameStream;
    fileNameStream << prefix << "_" << std::setfill('0') << std::setw(2) << segmentId << "." << extension;

    fullPath_ = path;
    fullPath_ /= fileNameStream.str();
}


Segment::~Segment()
{
    if (mappedMem_)
    {
        munmap(mappedMem_, mappedSize_);
    }

    if (fd_ >= 0)
    {
        close(fd_);
    }
}


//
// Load an existing segment.
//

void Segment::load()
{
    // Open the file.
    fd_ = open(fullPath_.c_str(), O_RDWR);
    if (fd_ == -1)
        throw RuntimeException(std::string("can't open ") + fullPath_.string() + ": " + strerror(errno));
    
    // Read the header.
    struct SegmentHeader hdr;
    int rc = read(fd_, &hdr, sizeof(struct SegmentHeader));
    if (rc == -1)
        throw RuntimeException(std::string("can't read ") + fullPath_.string() + ": " + strerror(errno));
        
    else if (rc != sizeof(struct SegmentHeader))
        throw RuntimeException(std::string("can't read ") + fullPath_.string() + ": not enough data");

    // Check the file magic.
    if (strncmp(hdr.magic, DELTACODE_PDB_MAGIC, sizeof(hdr.magic)))
        throw RuntimeException(std::string("can't read ") + fullPath_.string() + ": bad magic");

    if (hdr.versionMajor != DELTACODE_PDB_VERSION_MAJOR || hdr.versionMinor != DELTACODE_PDB_VERSION_MINOR)
        throw RuntimeException(std::string("can't read ") + fullPath_.string() + ": unreadable file format version");

    if (hdr.mappedSize >= 0x1000000000ULL || hdr.memOffset >= hdr.mappedSize)
        throw RuntimeException(std::string("can't read ") + fullPath_.string() + ": corrupted");

    // Map the memory.
    mappedMem_ = mmap(hdr.mappedAddress, hdr.mappedSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd_, 0);
    if (mappedMem_ == MAP_FAILED)
        throw RuntimeException(std::string("can't mmap ") + fullPath_.string() + ": " + strerror(errno));

    // Get the fields.
    mappedSize_ = hdr.mappedSize;
    hdr_ = reinterpret_cast<struct SegmentHeader *>(mappedMem_);
    minLevel_ = hdr_->minLevel;
    segmentMaxLevel_ = hdr_->segmentMaxLevel;
    storeMaxLevel_ = hdr_->storeMaxLevel;
    maxDepth_ = segmentMaxLevel_ - minLevel_;
    numSegments_ = hdr_->numSegments;

    int statusEntries = 1 << (segmentMaxLevel_ - minLevel_ + 1);
    int statusWords = (statusEntries + 31) / 32;    // Round up.
    size_t headerSize = sizeof(struct SegmentHeader) - sizeof(SegmentHeader::blockStatus);
    size_t statusTableSize = statusWords * sizeof(uint64_t);
    blockStatus_ = reinterpret_cast<uint64_t *>(reinterpret_cast<uint8_t *>(mappedMem_) + headerSize);
    blockMem_ = reinterpret_cast<void *>(reinterpret_cast<uint8_t *>(mappedMem_) + headerSize + statusTableSize);

    // Construct the free list.
    makeFreeList();
}


//
// Create a new segment file.
//

void Segment::create(int minLevel, int segmentMaxLevel, int storeMaxLevel)
{
    // How much memory are we going to need?
    int statusEntries = 1 << (segmentMaxLevel - minLevel + 1);
    int statusWords = (statusEntries + 31) / 32;    // Round up.
    size_t headerSize = sizeof(struct SegmentHeader) - sizeof(SegmentHeader::blockStatus);
    size_t statusTableSize = statusWords * sizeof(uint64_t);
    size_t memSize = 1 << segmentMaxLevel;
    size_t allocSize = headerSize + statusTableSize + memSize;

    // Make sure the preceding path exists.
    fs::create_directories(fullPath_.parent_path());
    
    // Open the backing file.
    std::string fullPathStr = fullPath_.string();
    int fd = open(fullPath_.c_str(), O_RDWR | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO);
    if (fd == -1)
        throw RuntimeException(std::string("can't open ") + fullPathStr + ": " + strerror(errno));

    // Extend the size if necessary.
    if (ftruncate(fd, allocSize) == -1)
    {
        close(fd);
        throw RuntimeException(std::string("can't set size of ") + fullPathStr + ": " + strerror(errno));
    }

    // Map the memory.
    void *mem = mmap(nullptr, allocSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem == MAP_FAILED)
    {
        close(fd);
        throw RuntimeException(std::string("can't mmap ") + fullPathStr + ": " + strerror(errno));
    }

    // Write the header.
    memset(mem, 0, headerSize + statusTableSize);
    hdr_ = reinterpret_cast<struct SegmentHeader *>(mem);
    strncpy(hdr_->magic, DELTACODE_PDB_MAGIC, sizeof(hdr_->magic));
    hdr_->versionMinor = DELTACODE_PDB_VERSION_MINOR;
    hdr_->versionMajor = 1;
    hdr_->minLevel = minLevel;
    hdr_->segmentMaxLevel = segmentMaxLevel;
    hdr_->storeMaxLevel = storeMaxLevel;
    hdr_->numSegments = 1;
    hdr_->mappedAddress = mem;
    hdr_->mappedSize = allocSize;
    hdr_->memOffset = headerSize + statusTableSize;

    // Close it.
    munmap(mem, allocSize);
    close(fd);
}


//
// Allocate some memory.
//
// level is the power of two size of this item.
//

void *Segment::allocate(int level)
{
    size_t offset = 0;

    // Clamp small allocations to the minimum size.
    if (level < minLevel_)
    {
        level = minLevel_;
    }

    int depth = segmentMaxLevel_ - level;

    // Can this segment accomodate something of this size?
    if (depth < 0)
        return nullptr;

    std::set<size_t> &bucket = freeList_[depth];

    // Find the first free block of the right size.
    if (!bucket.empty())
    {
        // Get the first free block of the right size.
        offset = *bucket.begin();
        bucket.erase(bucket.begin());
        setBlockStatus(depth, offset, BlockStatus::Allocated);
    }
    else
    {
        // Nothing available of the right size - make a block of the right size.
        // First search up through the buckets until we find a free block.
        int splitDepth = depth - 1;
        while (splitDepth >= 0 && freeList_.at(splitDepth).empty())
        {
            splitDepth--;
        }

        if (splitDepth < 0)
        {
            // No available space was found.
            return nullptr;
        }

        // We can split the first bucket we found.
        std::set<size_t> &splitBucket = freeList_.at(splitDepth);
        if (splitBucket.empty())
            throw RuntimeException("bucket is unexpectedly empty");

        offset = *splitBucket.begin();
        splitBucket.erase(splitBucket.begin());

        while (splitDepth < depth)
        {
            // Mark this bucket as split.
            setBlockStatus(splitDepth, offset, BlockStatus::Split);

            // Move down the tree.
            splitDepth++;
            offset *= 2;

            // Split the first half to us and the second half to free space.
            size_t spareOffset = offset + 1;
            freeList_.at(splitDepth).insert(spareOffset);
            setBlockStatus(splitDepth, spareOffset, BlockStatus::Free);
        }

        // Finally we can allocate at the chosen depth.
        setBlockStatus(depth, offset, BlockStatus::Allocated);
    }

    // Return the memory address we allocated.
    spaceUsed_ += 1 << (maxDepth_ - depth + minLevel_);
    void *addr = blockAddress(depth, offset);
    //std::cout << "alloc(" << level << ") = " << addr << std::endl;
    return addr;
}


//
// Free some memory.
//

void Segment::free(void *mem)
{
    // Search down the buddy bitmap for this memory.
    bool found = false;
    int blockDepth = 0;
    size_t memOffset = reinterpret_cast<uint8_t *>(mem) - reinterpret_cast<uint8_t *>(blockMem_);
    size_t offset = 0;
    while (blockDepth <= maxDepth_ && !found)
    {
        size_t checkOffset = memOffset >> (maxDepth_ - blockDepth + minLevel_);
        switch (getBlockStatus(blockDepth, checkOffset))
        {
        case BlockStatus::Free:
            throw RuntimeException("freeing unallocated persistent memory");
            break;

        case BlockStatus::Split:
            // Ok, it must be allocated below this depth. Keep going deeper.
            blockDepth++;
            break;

        case BlockStatus::Allocated:
            // Found it. Return it to the free list.
            offset = checkOffset;
            found = true;
            setBlockStatus(blockDepth, offset, BlockStatus::Free);
            break;
        }
    }

    if (!found)
        throw RuntimeException("freeing unallocated persistent memory");

    // Return the memory to the free list while trying to make buddies up the tree.
    bool done = false;
    int depth = blockDepth;
    while (depth > 0 && !done)
    {
        size_t buddyOffset = offset ^ 0x1;
        if (getBlockStatus(depth, buddyOffset) == BlockStatus::Free)
        {
            // My buddy's free as well. Remove it from the free list.
            freeList_.at(depth).erase(buddyOffset);
            
            // Move up to the next larger block size.
            depth--;
            offset /= 2;

            // Marking the parent as free.
            setBlockStatus(depth, offset, BlockStatus::Free);
        }
        else
        {
            // My buddy isn't free. We're at the largest block we can free for now
            done = true;
        }
    }

    // Add the largest free block we encountered to the free list.
    freeList_.at(depth).insert(offset);
    size_t spaceFreed = 1 << (maxDepth_ - blockDepth + minLevel_);
    spaceUsed_ -= spaceFreed;
    //std::cout << "free(" << mem << ") depth=" << blockDepth << ", " << spaceFreed << " bytes\n";
}


//
// Construct in-memory buckets of free lists of various sizes based
// on the buddy bitmap.
//

void Segment::makeFreeList()
{
    // Clear the free list.
    freeList_.clear();
    freeList_.resize(segmentMaxLevel_ - minLevel_ + 1);
    spaceUsed_ = 0;

    // Start making it recursively.
    makeFreeList(0, 0);
}


void Segment::makeFreeList(int depth, size_t offset)
{
    BlockStatus bs = getBlockStatus(depth, offset);
    switch (bs)
    {
        case BlockStatus::Free:
            // This node's empty, put it in the free list.
            freeList_.at(depth).insert(offset);
            break;
            
        case BlockStatus::Split:
            // The node's split. Search down.
            if (depth < maxDepth_)
            {
                makeFreeList(depth+1, offset*2);
                makeFreeList(depth+1, offset*2+1);
            }
            break;
            
        case BlockStatus::Allocated:
            // The block's full. Add it to the total space.
            spaceUsed_ += 1 << (maxDepth_ - depth + minLevel_);
            break;
    }
}


void Segment::setStoreMaxLevel(int v)
{
    storeMaxLevel_ = v;
    hdr_->storeMaxLevel = v;
}


void Segment::setNumSegments(int v)
{
    numSegments_ = v;
    hdr_->numSegments = v;
}


//
// Check if a file exists.
//

bool Segment::exists() const
{
    return fs::exists(fullPath_);
}


//
// Delete a segment file.
//

void Segment::deleteFile() const
{
    fs::remove(fullPath_);
}


} // namespace deltacode
