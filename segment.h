#pragma once

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <vector>
#include <set>
#include <experimental/filesystem>

#include "runtimeexception.h"


namespace fs = std::experimental::filesystem;


namespace deltacode {


//
// File header for segment files. Used internally.
//

struct SegmentHeader
{
    char     magic[16];         // Identifies this file type.
    uint32_t versionMinor;      // Minor version of this file encoding.
    uint32_t versionMajor;      // Major version of this file encoding.
    uint32_t minLevel;          // Power of two minimum depth of this segment.
    uint32_t segmentMaxLevel;   // Power of two maximum depth of this segment.
    uint32_t storeMaxLevel;     // Power of two maximum depth of all segments.
    uint32_t numSegments;       // The total number of segments.
    void    *mappedAddress;     // The address that this segment must be mapped to.
    size_t   mappedSize;        // Total memory mapped size.
    size_t   memOffset;         // Offset bytes to the start of memory data.
    uint64_t blockStatus[1];    // A variable size array of 2-bit buddy block statuses.
};


//
// A heap is made of a number of segments. Segments are areas of memory
// which are used for allocation. A heap usually contains multiple
// segments of different sizes. 
//
// Segments correspond to files on the disk used as backing store.
//
// Each time the heap runs out of space it allocates a new segment of a
// larger size than all the previous segments.
//

class Segment
{
private:
    // Indicates how much of a memory block is in use.
    enum class BlockStatus {
        Free = 0,   // Available for allocation.
        Split,      // The block is split in two. Look below it in the tree for more detail.
        Allocated   // The block is allocated.
    };

public:
    explicit Segment(const std::string &path, const std::string &prefix, const std::string &extension, int segmentId);
    ~Segment();

    // Load an existing segment file.
    void load();

    // Create a segment file.
    void create(int minLevel, int segmentMaxLevel, int storeMaxLevel);

    // Allocate some memory.
    // level is the power of two size of the item to allocate.
    void *allocate(int level);

    // Free some memory.
    void free(void *mem);

    // Get the index block for this segment. This is a block used to find the rest of
    // the items stored in the block.
    void *getIndexBlock(int level) { if (spaceUsed_) return blockMem_; else return allocate(level); }

    // Check if a segment file exists.
    bool exists() const;

    // Delete a segment file.
    void deleteFile() const;

    // Get information about this segment.
    int    getMinLevel() const        { return minLevel_; }
    int    getSegmentMaxLevel() const { return segmentMaxLevel_; }
    int    getStoreMaxLevel() const   { return storeMaxLevel_; }
    int    getMaxDepth() const        { return maxDepth_; }
    int    getNumSegments() const     { return numSegments_; }
    size_t getSize() const            { return 1UL << (maxDepth_ + minLevel_); }
    size_t getUsedSpace() const       { return spaceUsed_; }

    void   setStoreMaxLevel(int v);
    void   setNumSegments(int v);

    // Check if a memory address is inside our memory range.
    bool   isInMemoryRange(void *mem) { return mem >= blockMem_ && static_cast<size_t>(reinterpret_cast<uint8_t *>(mem) - reinterpret_cast<uint8_t *>(mappedMem_) ) < mappedSize_; }

private:
    // Make the free list based on the buddy bitmap.
    void makeFreeList();
    void makeFreeList(int depth, size_t offset);

    // Allocate a block recursively.
    void *blockAllocate(int allocDepth, int blockDepth, size_t blockOffset);

    // Get or set a block status.
    // 
    // Block statuses are stored in an array with exponentially increasing 
    // width as the depth increases. Depth zero has a single item. Depth 
    // one has two items. Depth three has four items, depth four eight items
    // and so on.
    //
    // This is all represented as a single array. Since BlockStatuses can be
    // represented as a two bit value they're packed into an array of uint64_t.

    BlockStatus getBlockStatus(int depth, size_t offset)
    {
        size_t width = 1 << depth;
        size_t index = width + offset;
        size_t wordIndex = index >> 5;
        int    wordOffset = index & 0x1f;
        int    bitOffset = wordOffset << 1;

        if (wordIndex >= (1U << (segmentMaxLevel_ - minLevel_ + 1)))
            throw RuntimeException("invalid block get depth");

        if (offset >= width)
            throw RuntimeException("invalid block get offset");

        uint64_t bs = (blockStatus_[wordIndex] >> bitOffset) & 0x3;

        //std::cout << "getBlockStatus(" << depth << ", " << offset << ") " << wordIndex << "," << bitOffset << "," << std::hex << *blockStatus_ << " = " << std::dec << bs << std::endl;
        return static_cast<BlockStatus>(bs);
    }

    void setBlockStatus(int depth, size_t offset, BlockStatus bs)
    {
        size_t   width = 1 << depth;
        size_t   index = width + offset;
        size_t   wordIndex = index >> 5;
        int      wordOffset = index & 0x1f;
        int      bitOffset = wordOffset << 1;
        uint64_t mask = ~(static_cast<uint64_t>(0x3) << bitOffset);

        if (wordIndex >= (1U << (segmentMaxLevel_ - minLevel_ + 1)))
            throw RuntimeException("invalid block set");

        if (offset >= width)
            throw RuntimeException("invalid block set offset");

        //std::cout << "setBlockStatus(" << depth << ", " << offset << ", " << static_cast<int>(bs) << ") " << wordIndex << "," << bitOffset << "," << std::hex << *blockStatus_ << std::dec << std::endl;
        blockStatus_[wordIndex] = (blockStatus_[wordIndex] & mask) | (static_cast<uint64_t>(bs) << bitOffset);
    }

    // Find the address of a memory block in this segment.
    void *blockAddress(int depth, size_t offset)
    {
        return reinterpret_cast<uint8_t *>(blockMem_) + (1 << (segmentMaxLevel_ - depth)) * offset;
    }

private:
    fs::path  fullPath_;           // The path to the segment file.
    int       minLevel_;           // Power of two minimum depth of this segment.
    int       segmentMaxLevel_;    // Power of two maximum depth of this segment.
    int       storeMaxLevel_;      // Power of two maximum depth of all segments.
    int       maxDepth_;           // segmentMaxLevel_ - minLevel_.
    int       numSegments_;        // The number of segment files.
    int       fd_;                 // The memory mapped file.
    struct    SegmentHeader *hdr_; // Contains all the private header fields.
    void     *mappedMem_;          // The segment's memory mapped memory area.
    size_t    mappedSize_;         // Size of the mapped memory area.
    size_t    spaceUsed_;          // The space currently used out of the available space.
    uint64_t *blockStatus_;        // Packed 2-bit array of block statuses. See getBlockStatus() for more info.
    void     *blockMem_;           // The memory area to be allocated from.
    std::vector< std::set<size_t> > freeList_; // Buckets of free blocks of various sizes.
};

} // namespace deltacode
