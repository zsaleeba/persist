//
// Persistent store for deltacode. This is a standard C++ allocator which
// allocates data in file-backed mmap() storage. When the program is
// resumed it remaps all the memory to the same addresses and is able to
// resume using the data structures with very low overhead.
//

#pragma once

#include <limits>
#include <iostream>
#include <vector>
#include <memory>

#include "heap.h"


namespace deltacode {


template <class T>
class Persist 
{
public:
    // Type definition.
    using value_type    = T;

    
    //
    // Constructors and destructor.
    //

    explicit Persist(Heap &heap) noexcept
    {
    }

    template <class U> Persist(Persist<U> const&) noexcept 
    {
    }

    // Allocate but don't initialize n elements of type T.
    value_type *allocate(std::size_t n) 
    {
        return static_cast<value_type *>(heap_.allocate(n * sizeof(value_type)));
    }

    // Deallocate storage p of deleted elements.
    void deallocate (value_type *p, std::size_t) 
    {
        heap_.free((void*)p);
    }

private:
    // Where to store the data.
    Heap &heap_;
};

// Return that all specializations of this allocator are interchangeable.
template <class T1, class T2>
bool operator== (const Persist<T1>&, const Persist<T2>&) throw() 
{
    return true;
}

template <class T1, class T2>
bool operator!= (const Persist<T1>&x, const Persist<T2>&y) throw() 
{
    return !(x == y);
}

} // namespace deltacode
