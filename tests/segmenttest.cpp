#include <iostream>
#include <gtest/gtest.h>
#include <experimental/filesystem>
#include <experimental/random>

#include "segment.h"


using namespace deltacode;
namespace fs = std::experimental::filesystem;


#define MAX_ALLOC_LEVEL 17
#define MIN_ALLOC_LEVEL 4


class SegmentFixture : public ::testing::Test 
{
public:
    SegmentFixture() : seg_("testdir", "segmenttest", "persist", 0)
    {
    }

    void SetUp()
    {
        seg_.load();
    }

public:
    deltacode::Segment seg_;
};


TEST(SegmentTest, Create) 
{
    Segment s("testdir", "segmenttest", "persist", 0);
    s.create(4, MAX_ALLOC_LEVEL, MAX_ALLOC_LEVEL);
}


TEST_F(SegmentFixture, Load) 
{
    ASSERT_EQ(seg_.getMinLevel(), MIN_ALLOC_LEVEL);
    ASSERT_EQ(seg_.getSegmentMaxLevel(), MAX_ALLOC_LEVEL);
    ASSERT_EQ(seg_.getStoreMaxLevel(), MAX_ALLOC_LEVEL);
    ASSERT_EQ(seg_.getMaxDepth(), MAX_ALLOC_LEVEL - MIN_ALLOC_LEVEL);
    ASSERT_EQ(seg_.getNumSegments(), 1);
}



TEST_F(SegmentFixture, AllocOne) 
{
    void *first = seg_.allocate(8);
    ASSERT_NE(first, nullptr);
    seg_.free(first);
}


std::vector<void *> blocks;

TEST_F(SegmentFixture, AllocUp) 
{
    for (int i = MIN_ALLOC_LEVEL; i < MAX_ALLOC_LEVEL; i++)
    {
        void *mem = seg_.allocate(i);
        ASSERT_NE(mem, nullptr);
        blocks.push_back(mem);
    }
}


TEST_F(SegmentFixture, FreeUp) 
{
    for (auto mem : blocks)
    {
        seg_.free(mem);
    }

    ASSERT_EQ(seg_.getUsedSpace(), 0);
    blocks.clear();
}


TEST_F(SegmentFixture, AllocDown) 
{
    blocks.clear();

    for (int i = MAX_ALLOC_LEVEL - 1; i >= MIN_ALLOC_LEVEL; i--)
    {
        void *mem = seg_.allocate(i);
        ASSERT_NE(mem, nullptr);
        blocks.push_back(mem);
    }
}


TEST_F(SegmentFixture, FreeDown) 
{
    for (auto mem : blocks)
    {
        seg_.free(mem);
    }

    ASSERT_EQ(seg_.getUsedSpace(), 0);
    blocks.clear();
}


TEST_F(SegmentFixture, AllocSmall) 
{
    blocks.clear();

    for (int i = 0; i < (1 << (MAX_ALLOC_LEVEL - MIN_ALLOC_LEVEL)); i++)
    {
        void *mem = seg_.allocate(MIN_ALLOC_LEVEL);
        ASSERT_NE(mem, nullptr);
        blocks.push_back(mem);
    }
}


TEST_F(SegmentFixture, FreeSmall) 
{
    for (auto mem : blocks)
    {
        seg_.free(mem);
    }

    ASSERT_EQ(seg_.getUsedSpace(), 0);
    blocks.clear();
}


TEST_F(SegmentFixture, AllocRandom) 
{
    size_t maxSpace = seg_.getSize() / 4;
    size_t totalAllocated = 0;
    blocks.clear();

    for (int i = 0; i < 500000; i++)
    {
        // How much are we going to allocate?
        int allocLevel = std::experimental::randint(MIN_ALLOC_LEVEL, MAX_ALLOC_LEVEL - 4);
        size_t allocSpace = 1UL << allocLevel;
        totalAllocated += allocSpace;

        // Do we need to free some space first?
        while (seg_.getUsedSpace() >= maxSpace)
        {
            // Free a random block.
            size_t blockNo = std::experimental::randint(static_cast<size_t>(0), blocks.size() - 1);
            seg_.free(blocks[blockNo]);
            blocks.erase(blocks.begin() + blockNo);
        }

        // Allocate it.
        //std::cout << "alloc(" << allocLevel << ") " << (1 << allocLevel) << ", " << seg_.getUsedSpace() << std::endl;
        void *mem = seg_.allocate(allocLevel);
        ASSERT_NE(mem, nullptr);
        blocks.push_back(mem);
    }
}


TEST_F(SegmentFixture, FreeRandom) 
{
    for (auto mem : blocks)
    {
        seg_.free(mem);
    }

    ASSERT_EQ(seg_.getUsedSpace(), 0);
    blocks.clear();
}


TEST_F(SegmentFixture, CreatePersistent)
{
    Segment s("testdir", "segmenttest", "persist", 0);
    s.create(4, MAX_ALLOC_LEVEL, MAX_ALLOC_LEVEL);
    seg_.load();
    uint8_t *block = reinterpret_cast<uint8_t *>(seg_.getIndexBlock(8));
    for (size_t i = 0; i < 256; i++)
    {
        block[i] = i;
    }
}


TEST_F(SegmentFixture, LoadPersistent)
{
    uint8_t *block = reinterpret_cast<uint8_t *>(seg_.getIndexBlock(8));
    for (size_t i = 0; i < 256; i++)
    {
        ASSERT_EQ(block[i], i);
    }
}