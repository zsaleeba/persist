#include <iostream>
#include <gtest/gtest.h>
#include <experimental/filesystem>

#include "heap.h"


#define BLOCK_SIZE_TEST 256
#define MAX_SEGMENT_TEST 1024


using namespace deltacode;
namespace fs = std::experimental::filesystem;

class HeapFixture : public ::testing::Test 
{
public:
    HeapFixture() : heap_("testdir", "heaptest", "persist", sizeof(void *), false, 65536)
    {
    }

public:
    deltacode::Heap heap_;
};


void *ib = nullptr;

TEST(HeapTest, Create)
{
    fs::remove_all("testdir");
    Heap h("testdir", "heaptest", "persist", sizeof(uint8_t *), true);
    ib = h.getIndexBlock();
    ASSERT_NE(ib, nullptr);
    *reinterpret_cast<uint64_t *>(ib) = 0x123456789abcdefULL;
}


TEST(HeapTest, Load)
{
    Heap h("testdir", "heaptest", "persist", sizeof(uint8_t *), false);
    void *newIb = h.getIndexBlock();
    ASSERT_EQ(newIb, ib);
    ASSERT_EQ(*reinterpret_cast<uint64_t *>(newIb), 0x123456789abcdefULL);
}


TEST(HeapTest, Alloc)
{
    Heap h("testdir", "heaptest", "persist", sizeof(uint8_t *), false);
    void *newIb = h.getIndexBlock();
    
    void *mem = h.allocate(BLOCK_SIZE_TEST);
    uint8_t *pattern = reinterpret_cast<uint8_t *>(mem);
    for (int i = 0; i < BLOCK_SIZE_TEST; i++)
    {
        pattern[i] = i;
    }

    uint8_t **ibp = reinterpret_cast<uint8_t **>(newIb);
    *ibp = pattern;
}


TEST(HeapTest, AllocCheck)
{
    Heap h("testdir", "heaptest", "persist", sizeof(uint8_t *), false);
    void *newIb = h.getIndexBlock();
    uint8_t **ibp = reinterpret_cast<uint8_t **>(newIb);
    
    uint8_t *pattern = reinterpret_cast<uint8_t *>(*ibp);
    for (int i = 0; i < BLOCK_SIZE_TEST; i++)
    {
        ASSERT_EQ(pattern[i], i);
    }
}


TEST_F(HeapFixture, AllocFree) 
{
    void *mem = heap_.allocate(1234);
    ASSERT_NE(mem, nullptr);
    heap_.free(mem);
}


typedef uint8_t oneBlock[BLOCK_SIZE_TEST];
typedef oneBlock *oneBlock_p;
typedef oneBlock_p blockArray[MAX_SEGMENT_TEST];
oneBlock pattern;


TEST_F(HeapFixture, Cleanup)
{
    fs::remove_all("testdir");
}


TEST_F(HeapFixture, AllocateMultipleSegments) 
{
    blockArray *blocks;

    // Create the pattern.
    for (int i = 0; i < BLOCK_SIZE_TEST; i++)
    {
        pattern[i] = i;
    }

    // Allocate an array to store the segments.
    blocks = reinterpret_cast<blockArray *>(heap_.allocate(sizeof(blockArray)));
    
    // Store a pointer to the block array in the index block.
    blockArray **ib = reinterpret_cast<blockArray **>(heap_.getIndexBlock());
    *ib = blocks;

    // Allocate some blocks.
    for (int i = 0; i < MAX_SEGMENT_TEST; i++)
    {
        oneBlock_p block = reinterpret_cast<oneBlock_p>(heap_.allocate(BLOCK_SIZE_TEST));
        (*blocks)[i] = block;

        memcpy(block, pattern, BLOCK_SIZE_TEST);

        // Access all the previous blocks because I'm suspicious.
        for (int j = 0; j <= i; j++)
        {
            oneBlock_p b = (*blocks)[j];
            uint8_t v = (*b)[0];
            ASSERT_EQ(v, 0);
        }
    }
}


TEST_F(HeapFixture, CheckMultipleSegments)
{
    // Get the pointer to the block array.
    blockArray **ib = reinterpret_cast<blockArray **>(heap_.getIndexBlock());
    blockArray *blocks = *ib;

    // Check the block contents.
    for (int i = 0; i < MAX_SEGMENT_TEST; i++)
    {
        ASSERT_EQ(memcmp(&pattern, (*blocks)[i], BLOCK_SIZE_TEST), 0);
    }
}


TEST_F(HeapFixture, FreeMultipleSegments)
{
    // Get the pointer to the block array.
    blockArray **ib = reinterpret_cast<blockArray **>(heap_.getIndexBlock());
    blockArray *blocks = *ib;

    // Free them all.
    for (int i = 0; i < MAX_SEGMENT_TEST; i++)
    {
        heap_.free((*blocks)[i]);
    }

    // Free the block array.
    heap_.free(blocks);
}