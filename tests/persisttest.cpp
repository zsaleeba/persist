#include <iostream>
#include <gtest/gtest.h>
#include <experimental/filesystem>
#include <ctype.h>

#include "persist.h"


using namespace deltacode;
namespace fs = std::experimental::filesystem;

class PersistFixture : public ::testing::Test 
{
public:
    PersistFixture() : heap_("testdir", "heaptest", "persist", sizeof(void *), false, 65536)
    {
    }

public:
    deltacode::Heap heap_;
};


TEST_F(PersistFixture, AllocFreeRawHeap) 
{
    int *mem = reinterpret_cast<int *>(heap_.allocate(1234));
    ASSERT_NE(mem, nullptr);
    heap_.free(mem);
}


TEST_F(PersistFixture, StoreRawHeap) 
{
    int *mem = reinterpret_cast<int *>(heap_.allocate(sizeof(int) * 1234));
    ASSERT_NE(mem, nullptr);

    int **indexBlock = reinterpret_cast<int **>(heap_.getIndexBlock());
    ASSERT_NE(indexBlock, nullptr);
    *indexBlock = mem;

    for (int i = 0; i < 1234; i++)
    {
        mem[i] = i;
    }
}


TEST_F(PersistFixture, RecallRawHeap) 
{
    int **indexBlock = reinterpret_cast<int **>(heap_.getIndexBlock());
    ASSERT_NE(indexBlock, nullptr);
    int *mem = *indexBlock;
    ASSERT_NE(mem, nullptr);

    for (int i = 0; i < 1234; i++)
    {
        ASSERT_EQ(mem[i], i);
    }

    heap_.free(mem);
}


TEST_F(PersistFixture, StoreCollection) 
{
    Persist<int> percy(heap_);
    int **indexBlock = reinterpret_cast<int **>(heap_.getIndexBlock());
    ASSERT_NE(indexBlock, nullptr);

    int *mem = percy.allocate(1234);
    ASSERT_NE(mem, nullptr);

    *indexBlock = mem;

    for (int i = 0; i < 1234; i++)
    {
        mem[i] = i;
    }
}


TEST_F(PersistFixture, RecallCollection)
{
    Persist<int> percy(heap_);
    int **indexBlock = reinterpret_cast<int **>(heap_.getIndexBlock());
    ASSERT_NE(indexBlock, nullptr);

    int *mem = *indexBlock;

    for (int i = 0; i < 1234; i++)
    {
        ASSERT_EQ(mem[i], i);
    }

    heap_.free(mem);
}


int main(int argc, char **argv) 
{
    ::testing::InitGoogleTest(&argc, argv);

    // Delete any leftovers from previous runs.
    fs::remove_all("testdir");

    int result = RUN_ALL_TESTS();

    // Delete our mess.
    fs::remove_all("testdir");

    return result;
}
