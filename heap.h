#pragma once

#include <cstdint>
#include <vector>
#include <memory>
#include <mutex>
#include <memory>

#include "segment.h"


namespace deltacode {


//
// A heap is made of a number of segments. Segments are areas of memory
// which are used for allocation. A heap usually contains multiple
// segments of different sizes. 
//
// Segments correspond to files on the disk used as backing store.
//
// Each time the heap runs out of space it allocates a new segment of a
// larger size than all the previous segments.
//

class Heap
{
private:
    enum
    {
        MIN_ALLOC_LEVEL = 4,      // 2^4 = 16. Minimum alloc size is 16 bytes.
        DEFAULT_ALLOC_LEVEL = 20, // 2^20 = 1MB. Default size of the initial heap.
        LARGE_ALLOC_MULTIPLE = 2  // 2^2 = 4. When allocating a segment larger than any we've seen before
                                  // allocate this number of times as much spare space.
    };
    
public:
    // Constructor.
    explicit Heap(const std::string &path, const std::string &prefix, const std::string &extension, size_t indexBlockSize, bool clearStore, size_t initialFileSize = (1 << DEFAULT_ALLOC_LEVEL));

    // Allocate some memory.
    // level is the power of two size of the item to allocate.
    void *allocate(size_t len);

    // Free some memory.
    void free(void *mem);

    // Get the index block of memory.
    void *getIndexBlock() { return indexBlock_; }

private:
    // Delete an existing data store.
    void deleteDataStore();

    // Load an existing data store.
    void loadDataStore();

    // Create a new data store.
    void createDataStore(size_t initialFileSize);

    // Return the next power two which is greater than or equal to the given number.
    uint64_t roundUpToPowerOfTwo(uint64_t v) const;

    // Find the bit position of a power of two value.
    int log2PowerOfTwo(uint64_t v) const;

    // Get the index block of memory.
    void getIndexBlockFirstTime(size_t len);

private:
    // Used to create paths.
    std::string      path_;
    std::string      prefix_;
    std::string      extension_;

    // Mutex required to access all of the below.
    std::mutex       mutex_;

    // The size of the next backing file to be created.
    int              nextSegmentLevel_;
    int              nextSegmentId_;

    // The set of segments of increasing sizes.
    std::vector< std::unique_ptr<Segment> > segments_;

    // Bucket sizes and the last bucket they were successfully allocated in.
    std::vector<int> lastAllocatedBucket_;

    // The user index block.
    void *           indexBlock_;
};

} // namespace deltacode
